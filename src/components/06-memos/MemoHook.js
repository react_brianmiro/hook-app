import React, { useMemo, useState } from 'react'
import { useCounter } from '../../hooks/useCounter';

import './memorize.css';

export const MemoHook = () => {

    const { counter, increment } = useCounter(500);

    const [show, setShow] = useState();

    const procesoPesado = (iteracciones) => {

        for (let i = 0; i < iteracciones; i++) {
            console.log('ahi vamos...');
        }
        return `${iteracciones} iteracciones realizadas. `;
    }

    const memoProcesoPesado = useMemo(() => procesoPesado( counter ), [ counter ] );

    return (
        <div>
            <h1>Memo Hook</h1>
            <h3>Counter : <small> {counter} </small> </h3>
            <hr />
            <p>{ memoProcesoPesado }</p>

            <button
                className="btn btn-primary"
                onClick={increment}
            >
                +1
            </button>
            <button
                className="btn btn-outline-primary"
                onClick={() => {
                    setShow(!show)
                }}
            >
                Show/Hide {JSON.stringify(show)}
            </button>
        </div>
    )
}
