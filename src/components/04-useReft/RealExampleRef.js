import React, { useState } from 'react';
import { MultipleCustomHook } from '../03-examples/MultipleCustomHook';

import './focus.css';

export const RealExampleRef = () => {

    const [show, setShow] = useState()

    return (
        <>
            <h1>Real Example</h1>
            <hr />

            {show && <MultipleCustomHook />}

            <button
                className="btn btn-outline-primary"
                onClick={() => setShow(!show) }
           >
            Show/Hide
        </button>

        </>
    )
}
