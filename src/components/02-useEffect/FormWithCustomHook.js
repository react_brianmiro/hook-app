
import React  from 'react'
import { UseForm } from '../../hooks/UseForm';


import './effect.css'

export const FormWithCustomHook = () => {

    const [ formValues, handleInputChange] = UseForm({
        name: '',
        email: '',
        password: ''

    });

    const { name, email ,password} = formValues;

    const handleOnSubmit = ( e ) =>  {
            e.preventDefault();
            console.log( formValues );
    }

    return (
        <form onSubmit={ handleOnSubmit }>
            <h1>FormWithCustomHook</h1>
            <hr />

            <div className="form-group">

                <input
                    type="text"
                    name="name"
                    className="form-control"
                    placeholder="Tu nombre"
                    autoComplete="off"
                    value={name}
                    onChange={handleInputChange}
                >
                </input>

            </div>

            <div className="form-group">

                <input
                    type="text"
                    name="email"
                    className="form-control"
                    placeholder="email@gmail.com"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChange}
                >
                </input>

            </div>

            <div className="form-group">

                <input
                    type="password"
                    name="password"
                    className="form-control"
                    placeholder="*****"
                    autoComplete="off"
                    value={ password }
                    onChange={handleInputChange}
                >
                </input>

            </div>
            <button  type="submit" className="btn btn-primary">Guardar</button>
        </form>
    )
}
